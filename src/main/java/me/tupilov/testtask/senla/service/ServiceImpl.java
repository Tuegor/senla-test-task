package me.tupilov.testtask.senla.service;

import me.tupilov.testtask.senla.dao.DAO;
import me.tupilov.testtask.senla.entity.Address;
import me.tupilov.testtask.senla.entity.Coords;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.HashMap;

@org.springframework.stereotype.Service
public class ServiceImpl implements Service {
    private DAO employeeDAO;
    private HashMap<Address,Coords> addressCache;
    private HashMap<Coords,Address> coordsCache;

    @Autowired
    public ServiceImpl(DAO employeeDAO, HashMap<Address, Coords> addressCache, HashMap<Coords, Address> coordsCache) {
        this.employeeDAO = employeeDAO;
        this.addressCache = addressCache;
        this.coordsCache = coordsCache;
    }

    @Override
    public Coords getCoords(Address theAddress) throws IOException {
        if (addressCache.containsKey(theAddress))
            return addressCache.get(theAddress);
        else
        {
            Coords theCoords = employeeDAO.getCoords(theAddress);
            addressCache.put(theAddress, theCoords);
            return theCoords;
        }
    }

    @Override
    public Address getAddress(Coords theCoords) throws IOException {
        if (coordsCache.containsKey(theCoords))
            return coordsCache.get(theCoords);
        else
        {
            Address theAddress = employeeDAO.getAddress(theCoords);
            coordsCache.put(theCoords, theAddress);
            return theAddress;
        }
    }


}

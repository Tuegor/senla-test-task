package me.tupilov.testtask.senla.service;

import me.tupilov.testtask.senla.entity.Address;
import me.tupilov.testtask.senla.entity.Coords;

import java.io.IOException;

public interface Service {

    public Coords getCoords(Address theAddress) throws IOException;

    public Address getAddress(Coords theCoords) throws IOException;
}

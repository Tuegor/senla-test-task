package me.tupilov.testtask.senla.dao;

import me.tupilov.testtask.senla.entity.Address;
import me.tupilov.testtask.senla.entity.Coords;

import java.io.IOException;

public interface DAO {
    public Coords getCoords(Address theAddress) throws IOException;

    public Address getAddress(Coords theCoords) throws IOException;
}

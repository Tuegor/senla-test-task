package me.tupilov.testtask.senla.dao;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.tupilov.testtask.senla.entity.Address;
import me.tupilov.testtask.senla.entity.Coords;
import me.tupilov.testtask.senla.rest.GeocoderException;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;

@Repository
public class DAOImpl implements DAO {
    private String basicURL = "https://geocode-maps.yandex.ru/1.x/?apikey=98c8c318-2460-4a33-986b-a6ff7a58bee3&format=json";

    @Override
    public Coords getCoords(Address theAddress) throws IOException {
        StringBuilder URLBuilder = new StringBuilder(basicURL);
        URLBuilder.append("&geocode=");
        URLBuilder.append(URLEncoder.encode(theAddress.getAddress(), "UTF-8"));

        URL url = new URL(URLBuilder.toString());
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(url).get("response").get("GeoObjectCollection").get("featureMember");
        if (!jsonNode.has(0))
            throw new GeocoderException("House not found - " + theAddress.getAddress());
        int index = 0;
        String kind = jsonNode.get(index).get("GeoObject").get("metaDataProperty").get("GeocoderMetaData").get("kind").asText();
        String[] pos = jsonNode.get(index).get("GeoObject").get("Point").get("pos").asText().split(" ");
        while(jsonNode.get(index) != null && !kind.equals("house"))
        {
            kind = jsonNode.get(index).get("GeoObject").get("metaDataProperty").get("GeocoderMetaData").get("kind").asText();
            pos = jsonNode.get(index).get("GeoObject").get("Point").get("pos").asText().split(" ");
            index++;
        }
        if (kind.equals("house"))
            return new Coords(pos[0], pos[1]);
        else
            throw new GeocoderException("House not found - " + theAddress.getAddress());
    }

    @Override
    public Address getAddress(Coords theCoords) throws IOException {
        StringBuilder URLBuilder = new StringBuilder(basicURL);
        URLBuilder.append("&kind=house");
        URLBuilder.append("&geocode=");
        URLBuilder.append(theCoords.getLongitude());
        URLBuilder.append(",");
        URLBuilder.append(theCoords.getLatitude());

        URL url = new URL(URLBuilder.toString());
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(url).get("response").get("GeoObjectCollection").get("featureMember");
        if (!jsonNode.has(0))
            throw new GeocoderException("House not found - " + theCoords.getLongitude() + "," + theCoords.getLatitude());
        String text = jsonNode.get(0).get("GeoObject").get("metaDataProperty").get("GeocoderMetaData").get("text").asText();
        return new Address(text);
    }
}

package me.tupilov.testtask.senla.rest;

import me.tupilov.testtask.senla.entity.Address;
import me.tupilov.testtask.senla.entity.Coords;
import me.tupilov.testtask.senla.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/api")
public class RestController {
    private Service employeeService;

    @Autowired
    public RestController(Service theEmployeeService)
    {
        employeeService = theEmployeeService;
    }

    @PostMapping("/address")
    public Coords getCoords(@RequestBody Address theAddress) throws IOException {
        Coords theCoords = employeeService.getCoords(theAddress);
        return theCoords;
    }

    @PostMapping("/coords")
    public Address getAddress(@RequestBody Coords theCoords) throws IOException {
        Address theAddress = employeeService.getAddress(theCoords);
        return theAddress;
    }
}

package me.tupilov.testtask.senla.rest;

public class GeocoderException extends RuntimeException{
    public GeocoderException(String message) {
        super(message);
    }

    public GeocoderException(String message, Throwable cause) {
        super(message, cause);
    }

    public GeocoderException(Throwable cause) {
        super(cause);
    }
}
